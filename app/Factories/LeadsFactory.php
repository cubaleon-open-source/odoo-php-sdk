<?php
namespace App\Factories;

use App\Domains\Lead;
use App\Exceptions\LeadException;
use App\Services\LeadCustomerService;
use App\Services\LeadPartnerService;

/**
 * Class LeadsFactory
 *
 * @package App\Factories
 */
class LeadsFactory
{
	/**
	 * @param $leadType
	 *
	 * @param $provider
	 *
	 * @return string
	 * @throws \App\Exceptions\LeadException
	 */
	public function make($leadType, $provider)
	{
		switch ($leadType) {
			case Lead::LEAD_CUSTOMER:
				return new LeadCustomerService($provider);

			case Lead::LEAD_PARTNER:
				return new LeadPartnerService($provider);
		}

		throw new LeadException('Lead type is not defined or does not exist');
	}
}
