<?php

namespace App\Providers;

use GuzzleHttp\Client;

/**
 * Class OdooServiceProvider
 *
 * @package App\Providers
 */
class OdooServiceProvider
{
	const AUTH_URI = 'api/auth/token/';

	/** @var string */
	private $url;

	/** @var string */
	private $login;

	/** @var string */
	private $password;

	/** @var string */
	private $db;

	/** @var string */
	private $token;

	/** @var Client */
	private $guzzleClient;

	/**
	 * @return \App\Providers\OdooServiceProvider
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function register(): OdooServiceProvider
	{
		/** Get all this from env */
		$this->url          = getenv('ODOO_URL');
		$this->login        = getenv('ODOO_LOGIN');
		$this->password     = getenv('ODOO_PASSWORD');
		$this->db           = getenv('ODOO_DB');
		$this->guzzleClient = $this->registerGuzzleInstance();
		$this->token        = $this->auth();

		return $this;
	}

	/**
	 * @return mixed
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	private function auth()
	{
		$params = [
			'query' => [
				'login'    => $this->login,
				'password' => $this->password,
				'db'       => $this->db,
			],
		];

		$response = $this->guzzleClient->request(
			'GET',
			static::AUTH_URI,
			$params
		);

		return (json_decode($response->getBody()))->access_token;
	}

	/**
	 * @return \GuzzleHttp\Client
	 */
	private function registerGuzzleInstance(): Client
	{
		/** @var Client $guzzleClient */
		$guzzleClient = new Client([
			'base_uri' => $this->url,
			'timeout'  => 2.0,
		]);

		return $guzzleClient;
	}


	/**
	 * @return string
	 */
	public function getUrl(): string
	{
		return $this->url;
	}

	/**
	 * @return string
	 */
	public function getLogin(): string
	{
		return $this->login;
	}

	/**
	 * @return string
	 */
	public function getPassword(): string
	{
		return $this->password;
	}

	/**
	 * @return string
	 */
	public function getDb(): string
	{
		return $this->db;
	}

	/**
	 * @return string
	 */
	public function getToken(): string
	{
		return $this->token;
	}

	/**
	 * @return Client
	 */
	public function getGuzzleClient(): Client
	{
		return $this->guzzleClient;
	}


}
