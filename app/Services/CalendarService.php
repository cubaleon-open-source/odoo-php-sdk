<?php


namespace App\Services;

use App\Domains\Calendar;
use App\Exceptions\CalendarException;
use App\Providers\OdooServiceProvider;

/**
 * Class CalendarService
 *
 * @package App\Services
 */
class CalendarService
{
	const URI = '/api/calendar.event/';

	/** @var OdooServiceProvider */
	private $odooServiceProvider;

	public function register($provider)
	{
		$this->odooServiceProvider = $provider;
	}

	/**
	 * @param \App\Domains\Calendar $calendar
	 *
	 * @return array
	 */
	public function validateAndFormat(Calendar $calendar): array
	{
		$array = [
			'name'        => $calendar->getName(),
			'email'       => $calendar->getEmail(),
			'start'       => $calendar->getStart(),
			'stop'        => $calendar->getStop(),
			'description' => $calendar->getDescription(),
		];

		return $array;
	}

	/**
	 * @param \App\Domains\Calendar $calendar
	 *
	 * @return array
	 * @throws \App\Exceptions\CalendarException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function setNewCalendarEvent(Calendar $calendar): array
	{
		$calendarArray = $this->validateAndFormat($calendar);

		if (!is_array($calendarArray)) {
			throw new CalendarException('Wrong Calendar please check the required parameters');
		}

		$body['form_params'] = $calendarArray;
		$body['headers']     = [
			'Content-Type' => 'application/x-www-form-urlencoded',
			'access_token' => $this->odooServiceProvider->getToken(),
		];

		$guzzleClient = $this->odooServiceProvider->getGuzzleClient();

		$lead = $guzzleClient->request(
			'POST',
			static::URI,
			$body
		);

		return json_decode($lead->getBody(), true);
	}
}