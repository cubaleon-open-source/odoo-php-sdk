<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 1/30/20
 * Time: 2:10 PM
 */

namespace App\Services;

use App\Providers\OdooServiceProvider;

/**
 * Class LeadService
 *
 * @package App\Services
 */
class LeadService
{
	const URI = '/api/crm.lead/';

	/** @var OdooServiceProvider */
	private $odooServiceProvider;

	public function __construct(OdooServiceProvider $odooServiceProvider)
	{
		$this->odooServiceProvider = $odooServiceProvider;
	}

	/**
	 * @param array $lead
	 *
	 * @return array
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function save(array $lead): array
	{
		$body['form_params'] = $lead;
		$body['headers'] = [
			'Content-Type' => 'application/x-www-form-urlencoded',
			'access_token' => $this->odooServiceProvider->getToken()
		];

		$guzzleClient = $this->odooServiceProvider->getGuzzleClient();

		$lead = $guzzleClient->request(
			'POST',
			static::URI,
			$body
		);

		return json_decode($lead->getBody(), true);
	}

	/**
	 * @param int $leadId
	 * @param int $leadNewStage
	 *
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function update(int $leadId, int $leadNewStage): void
	{
		$body['headers'] = [
			'Content-Type' => 'application/x-www-form-urlencoded',
			'access_token' => $this->odooServiceProvider->getToken()
		];

		$guzzleClient = $this->odooServiceProvider->getGuzzleClient();

		$guzzleClient->request(
			'PUT',
			static::URI . $leadId .'?stage_id=' . $leadNewStage,
			$body
		);
	}
}
