<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 1/30/20
 * Time: 2:21 PM
 */

namespace App\Services;

use App\Contracts\LeadContract;
use App\Domains\Lead;
use App\Exceptions\LeadException;
use App\Providers\OdooServiceProvider;

/**
 * Class LeadCustomerService
 *
 * @package App\Services
 */
class LeadCustomerService extends LeadService implements LeadContract
{
	const LEAD_TYPE = 'customer';

	/**
	 * LeadCustomerService constructor.
	 *
	 * @param OdooServiceProvider $odooServiceProvider
	 */
	public function __construct(OdooServiceProvider $odooServiceProvider)
	{
		parent::__construct($odooServiceProvider);
	}

	public function validateAndFormat(Lead $lead): array
	{
		$array = [
			'name'    => $lead->getName(),
			'email'   => $lead->getEmail(),
			'number'  => $lead->getNumber(),
			'address' => $lead->getAddress(),
			'kind'    => $lead->getType(),
			'funnel'  => $lead->getFunnel(),
		];

		return $array;
	}

	/**
	 * @param string $leadStage
	 *
	 * @return mixed
	 * @throws \App\Exceptions\LeadException
	 */
	public function leadStageToNumber(string $leadStage)
	{
		$leadArrayStage = [
			'opportunity' => 2,
			'proposition' => 3,
			'won'         => 4,
		];

		if (isset($leadArrayStage[$leadStage])) {

			return $leadArrayStage[$leadStage];
		}

		throw new LeadException('Invalid Lead stage');
	}

	/**
	 * @param \App\Domains\Lead $lead
	 *
	 * @return array
	 * @throws \App\Exceptions\LeadException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function setNewLead(Lead $lead): array
	{
		$leadArray = $this->validateAndFormat($lead);

		if (!is_array($leadArray)) {
			throw new LeadException('Wrong Lead please check the required parameters');
		}

		return $this->save($leadArray);
	}

	/**
	 * @param int    $leadId
	 * @param string $leadNewStage
	 *
	 * @throws \App\Exceptions\LeadException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function changeLeadStage(int $leadId, string $leadNewStage): void
	{
		$this->update($leadId, $this->leadStageToNumber($leadNewStage));
	}
}
