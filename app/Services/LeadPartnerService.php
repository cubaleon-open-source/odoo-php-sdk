<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 1/30/20
 * Time: 2:21 PM
 */

namespace App\Services;


use App\Contracts\LeadContract;
use App\Domains\Lead;
use App\Exceptions\LeadException;
use App\Providers\OdooServiceProvider;

class LeadPartnerService extends LeadService implements LeadContract
{
	const LEAD_TYPE = 'partner';

	/**
	 * LeadCustomerService constructor.
	 *
	 * @param OdooServiceProvider $odooServiceProvider
	 */
	public function __construct(OdooServiceProvider $odooServiceProvider)
	{
		parent::__construct($odooServiceProvider);
	}

	public function validateAndFormat(Lead $lead): array
	{
		/** TODO VALIDATE */
		return [];
	}

	/**
	 * @param \App\Domains\Lead $lead
	 *
	 * @return array
	 * @throws \App\Exceptions\LeadException
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 */
	public function setNewLead(Lead $lead): array
	{
		$leadArray = $this->validateAndFormat($lead);

		if (is_array($leadArray)) {

			return $this->save($leadArray);
		}

		throw New LeadException('Wrong Lead please check the required parameters');
	}

	public function changeLeadStage(int $leadId, string $leadNewStage): void
	{

	}
}
