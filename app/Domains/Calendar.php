<?php


namespace App\Domains;


class Calendar
{
	private $name;

	private $crmId;

	private $email;

	private $start;

	private $stop;

	private $description;

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param $name
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setName($name): Calendar
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCrmId()
	{
		return $this->crmId;
	}

	/**
	 * @param $crmId
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setCrmId($crmId): Calendar
	{
		$this->crmId = $crmId;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param $email
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setEmail($email): Calendar
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getStart()
	{
		return $this->start;
	}

	/**
	 * @param $start
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setStart($start): Calendar
	{
		$this->start = $start;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getStop()
	{
		return $this->stop;
	}

	/**
	 * @param $stop
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setStop($stop): Calendar
	{
		$this->stop = $stop;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDescription()
	{
		return $this->description;
	}

	/**
	 * @param $description
	 *
	 * @return \App\Domains\Calendar
	 */
	public function setDescription($description): Calendar
	{
		$this->description = $description;

		return $this;
	}
}