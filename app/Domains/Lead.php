<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 1/30/20
 * Time: 3:03 PM
 */

namespace App\Domains;


use phpDocumentor\Reflection\Types\This;

class Lead
{
	const LEAD_CUSTOMER = 'customer';
	const LEAD_PARTNER = 'partner';

	private $name;

	private $crmId;

	private $email;

	private $number;

	private $address;

	private $type;

	private $funnel;

	private $referral;

	/**
	 * @return mixed
	 */
	public function getCrmId()
	{
		return $this->crmId;
	}

	/**
	 * @param mixed $crmId
	 */
	public function setCrmId($crmId): void
	{
		$this->crmId = $crmId;
	}

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param $name
	 *
	 * @return \App\Domains\Lead
	 */
	public function setName($name): Lead
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param $email
	 *
	 * @return \App\Domains\Lead
	 */
	public function setEmail($email): Lead
	{
		$this->email = $email;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * @param $number
	 *
	 * @return \App\Domains\Lead
	 */
	public function setNumber($number): Lead
	{
		$this->number = $number;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getAddress()
	{
		return $this->address;
	}

	/**
	 * @param $address
	 *
	 * @return \App\Domains\Lead
	 */
	public function setAddress($address): Lead
	{
		$this->address = $address;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param $type
	 *
	 * @return \App\Domains\Lead
	 */
	public function setType($type): Lead
	{
		$this->type = $type;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFunnel()
	{
		return $this->funnel;
	}

	/**
	 * @param $funnel
	 *
	 * @return \App\Domains\Lead
	 */
	public function setFunnel($funnel): Lead
	{
		$this->funnel = $funnel;

		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getReferral()
	{
		return $this->referral;
	}

	/**
	 * @param mixed $referral
	 *
	 * @return \App\Domains\Lead
	 */
	public function setReferral($referral): Lead
	{
		$this->referral = $referral;

		return $this;
	}
}