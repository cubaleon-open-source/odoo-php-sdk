<?php
/**
 * Created by PhpStorm.
 * User: ml
 * Date: 1/30/20
 * Time: 2:18 PM
 */

namespace App\Contracts;

use App\Domains\Lead;

/**
 * Interface LeadContract
 *
 * @package App\Contracts
 */
interface LeadContract
{
	public function setNewLead(Lead $lead): array;

	public function changeLeadStage(int $leadId, string $leadNewStage): void ;

	public function validateAndFormat(Lead $lead): array;
}
